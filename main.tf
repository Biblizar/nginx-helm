resource "helm_release" "nginx" {
  name       = "my_nginx"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "nginx"
  version    = "1.25.4"

  namespace        = "nginx_demo"
  create_namespace = true
  atomic           = true

  values = [
    templatefile("${path.module}/values.yaml.tftpl", {
      numberOfReplicas = var.replicaCount
    })
  ]
}

variable "replicaCount" {
  description = "The number of Replicas for Nginx"
  default     = 1
  type        = number
}